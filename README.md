# README #

To list all the files present use :
$ls src/main/scala/

To run any one of them, use:
$ sbt "runMain <class_name_of_file>

Alternatively, after switching into the FigaroWork directory, 
$sbt
> run
(Please select the number corresponding to the file that you want to run)
> last run
(For debugging purposes)
> ^D
(Quit the sbt command line)

### What is this repository for? ###

All relevant materials consulted and implemented for learning purposes for Advanced Lab Task of CS4215, Spring 2018

### How do I get set up? ###

To run code, cd into the FigaroWork directory after cloning

### Contribution guidelines ###

https://www.cra.com/sites/default/files/pdf/Figaro_Tutorial.pdf
https://github.com/p2t2/figaro

### Who do I talk to? ###

Arijit Pramanik
A0179365N
e0268734@u.nus.edu